# Parametric 3D Printed Sex Toy Base

Clearly sex toys are best toys, but they can be annoying to wrangle with lube on you hands.
Lets fix that.

![triforce][triforce-img]

## Quick Start

### Hardware Needed

- You will need a male 1/4 NPT quick connect (sometimes called an "air chuck") - be aware they come in lots of sightly different models, I use the "IM" standard here in the US.
- You will need at least one matching female quick connect so you have something to attach it to.

You need one of the following for each edge, for example 6 for a hexagon adapter.
 - 1/4 inch carriage bolts, length depends on the toy.
 - 1/4 inch wing nut.


### Creating a custom STL
1. You need [OpenScad][openscad] installed, and I recommend using the Visual studio code extension to work with it.
2. Clone this repository
3. Open [ToyRetentionPlate.scad][retention-plate] and adjust the `PlateHeight`, `PlateWidth`, `PlateEdges`, and `ToyDiameter` variables to suit your toy. You can render the model with F6 and export it to an STL file with F7.
4. Open [BasePlate.scad](src/BasePlate.scad) and set the `PlateHeight` and `PlateEdges` variables to match the [retention plate][retention-plate], then render and export it.
5. Check the created STL measurements in your viewer of choice and sanity check them.
6. Print it!


## Todo

* Lightsaber handle base the adapters can attach to (for reasons)
* Shorter carriage bolts, both for better pictures and my own safety.
* Extract the base and retention plates into modules
* Clearly sex machines need to implement some kind of automated "tool" change system
* Add actual, helpful info to readme.


## Example Models (.STL files)

These happen to be for toys with a 2 in (50 mm) base, that don't have a large flange.
- [Base plate](docs/models/Tri-80mm-BasePlate.stl)
- [Toy Retention plate](docs/models/Tri-80mm-55mm_hole-ToyPlate.stl)



## Links

* [Kinky Makers Discord][km-discord]
* [Open Source Sex Machine][ossm]
* [Open Scad][openscad]


[retention-plate]: src/ToyRetentionPlate.scad
[triforce-img]: docs/images/triforce_or_master_swords_small.png
[openscad]: https://openscad.org/
[ossm]: https://kinkymakers.com/projects/open-source-sex-machine/
[km-discord]: https://discord.gg/MKysdqdK
