// This will eventuall have a BasePlate module, but not yet.
include <ToyModules.scad>;
include <BOSL2/std.scad>
include <BOSL2/threading.scad>
thread_fn=72;


// uncomment for nicely rounded curves before the final render
//$fn=120;


PlateWidth = 80;
PlateHeight=16;

PlateEdges = 3;
BoltDiameter = 8;
ToyDiameter = 65;

// this is me trying to make things scale to different sizes, always sanity check your model before printing!
pr1 = PlateWidth/4;
pr2 = PlateWidth/(PlateEdges*1.5);
pr3 = PlateWidth/(PlateEdges*2);



difference(){
    union(){
        translate([0, 0, -2]) {
            radial_hull(edgeCount=PlateEdges,l=PlateWidth){
            cylinder(r=pr1,h=2);
            }
        }
        radial_hull(edgeCount=PlateEdges,l=PlateWidth){
            cylinder(r1=pr1,r2=pr2,h=PlateHeight*.3);
        }
        radial_hull(edgeCount=PlateEdges,l=PlateWidth*.3,r_offset=1){
            cylinder(
               r1=pr1,r2=pr3,
                h=PlateHeight*.8);
        }
        cylinder(r=12,h=PlateHeight,$fn=6);
    }

    radial_place(edgeCount=PlateEdges,l=PlateWidth){
        translate([0, 0, -5]) {
            color("red")
            Punch(d=BoltDiameter,h=PlateHeight*2);
        }
    }

    translate([0,0,(PlateHeight)])
    rotate([0,180,0])
    color("green")
    npt_threaded_rod(size=1/4,internal=true,$fn=thread_fn);
}
