// These are just defaults, you should not need to adjust this file.

// thickness of the generated plate
PlateHeight = 10;
// Width of the plate
PlateWidth = 100;

// Edge buffer
EdgeWidth = 5;

// mm diameter of the corner bolt holes, if square you don't need to compensate for
// shinking circles but you should still oversize a bit for printing issues.
BoltDiameter = 8;

LessEdge = EdgeWidth  *2;



module show_toy(d,h=80){
    color("purple")
    translate([0, 0, 0]) {
        cylinder(d=d, h=h);

    }
}




module radial_place(l=10,edgeCount=4,r_offset=0){
    cornerOffset = l/2;
    initial_rotatiion = r_offset * ((360/edgeCount)/2);
    rotate([0,0,initial_rotatiion]){
        for (i=[0:edgeCount-1]) {
            xr =i* (360/edgeCount);
            rotate(xr,[0, 0, 1])
            translate([0, cornerOffset, 0])
            children();
        }
    }
}

module radial_hull(l,edgeCount,r_offset=0){
    hull(){
       radial_place(l=l,edgeCount=edgeCount,r_offset=r_offset)
           children();
    }
}

module Punch(hole="round",d=1,h=10,center=true){
        if(hole=="round"){
            cylinder(h=h,d=d,center=center);
            }
        else if(hole=="square"){
            cube([d,d,h],center=center);
            }
    }

// function overlap(val) = val-0.001;
function CornerOffset(w=PlateWidth,ew=EdgeWidth) = w/2 - (ew *2);

module CornerHoles(h=PlateHeight*1.5,d=BoltDiameter,cirOffset=CornerOffset(),hole="round"){
// round and reinforce the corners
translate([cirOffset,cirOffset,0])
Punch(h=h,d=d,hole=hole);
    translate([-cirOffset,cirOffset,0])
Punch(h=h,d=d,hole=hole);
    translate([cirOffset,-cirOffset,0])
Punch(h=h,d=d,hole=hole);
    translate([-cirOffset,-cirOffset,0])
Punch(h=h,d=d,hole=hole);
}



module plate(h=6,w=100,bevel=10){
    hull(){
    cornerPoint = w/2 - w;
    // round and reinforce the corners
    translate([cornerPoint ,cornerPoint ,0])
    cylinder(h=h,r=bevel,center=true);

    // round and reinforce the corners
    translate([-cornerPoint,cornerPoint,0])
    cylinder(h=h,r=bevel,center=true);

    // round and reinforce the corners
    translate([cornerPoint,-cornerPoint,0])
    cylinder(h=h,r=bevel,center=true);

    // round and reinforce the corners
    translate([-cornerPoint,-cornerPoint,0])
    cylinder(h=h,r=bevel,center=true);
    }
}


module xplate(h=6,w=100,bevel=10,xout=35,xoff=60){
    difference(){
    plate(h=h,w=w,bevel=bevel);
    translate([xoff ,0])
    sphere(xout);
    translate([0,xoff,0])
    sphere(xout);
    translate([0,-xoff,0])
    sphere(xout);
    translate([-xoff ,0,0])
    sphere(xout);
    };

}
