include <ToyModules.scad>;

PlateWidth = 80;
PlateHeight=4;
PlateEdges = 3;
ToyDiameter = 55;
BoltDiameter = 7;

// holes are square for carrige bolt heads
BoltHoleShape = "square";



// this is me trying to make things scale to different sizes, always sanity check your model before printing!
pr1 = PlateWidth/4;
pr2 = PlateWidth/(PlateEdges*1.5);
pr3 = PlateWidth/(PlateEdges*2);
$fn = 120;

//show_toy(d=ToyDiameter);

difference(){
    radial_hull(edgeCount=PlateEdges,l=PlateWidth){
        cylinder(r=pr1,h=PlateHeight);
    }
    radial_place(edgeCount=PlateEdges,l=PlateWidth){
        translate([0, 0, -1]) {
            color("red")
            Punch(d=BoltDiameter,h=PlateHeight*4,hole=BoltHoleShape);
        }
    }
    translate([0, 0, -PlateHeight]) {
    color("green")
    cylinder(d=ToyDiameter,h=PlateHeight*3);
    }
}
